﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GraniteHouse.Data.Migrations
{
    public partial class ProductModelFromGithub : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_SpecialTags_SpecialTagId",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "SpecialTagId",
                table: "Products",
                newName: "SpecialTagsID");

            migrationBuilder.RenameIndex(
                name: "IX_Products_SpecialTagId",
                table: "Products",
                newName: "IX_Products_SpecialTagsID");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_SpecialTags_SpecialTagsID",
                table: "Products",
                column: "SpecialTagsID",
                principalTable: "SpecialTags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_SpecialTags_SpecialTagsID",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "SpecialTagsID",
                table: "Products",
                newName: "SpecialTagId");

            migrationBuilder.RenameIndex(
                name: "IX_Products_SpecialTagsID",
                table: "Products",
                newName: "IX_Products_SpecialTagId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_SpecialTags_SpecialTagId",
                table: "Products",
                column: "SpecialTagId",
                principalTable: "SpecialTags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
